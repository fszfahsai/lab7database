package th.ac.tu.siit.lab7database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


		// this is a class that manage the structure of data table
	public class DBHelper extends SQLiteOpenHelper {
		
		private static final String DBNAME = "contacts.db";
		private static final int DBVERSION = 1;
		
		public DBHelper(Context ctx) {
			super(ctx, DBNAME, null, DBVERSION);//dbversion help to control database
		}

		@Override//called when the application is newly installed
		//no database file in the internal storage
		public void onCreate(SQLiteDatabase db) {
			//the primary key of the table need to be "_id"
			//when we want to use this table with ListView
			String sql = "CREATE TABLE contacts (" +
					"_id integer primary key autoincrement, " +
					"ct_name text not null, " +
					"ct_phone text not null, " +
					"ct_type integer default 0, " +
					"ct_email text not null);";
			db.execSQL(sql);		
		}

		@Override//called when the database file exists,
		//but the DBVERSION was increased
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			//Upgrade by removing the current table and recreate a new one
			//this is Not a practical way
			//you should use ALTER TABLE when you need to change the structure of the table
			String sql = "DROP TABLE IF EXISTS contacts;";
			db.execSQL(sql);
			this.onCreate(db);
		}

	}
